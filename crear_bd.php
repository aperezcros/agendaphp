<?php
error_reporting(E_ALL);ini_set('display_errors', 1);
// Tamaño de los campos en la tabla
define("TAM_NOMBRE",     40);  // Tamaño del campo Nombre
define("TAM_APELLIDOS",  60);  // Tamaño del campo Apellidos
define("TAM_TELEFONO",   9);  // Tamaño del campo Teléfono
define("TAM_MAIL",     50);  // Tamaño del campo Correo

//variables
$crear_Tabla= "CREATE TABLE IF NOT EXISTS agenda(
 	id INTEGER PRIMARY KEY AUTOINCREMENT,
    nombre VARCHAR(" . TAM_NOMBRE . "),
    apellidos VARCHAR(" . TAM_APELLIDOS . "), 
    telefono INT(" . TAM_TELEFONO . "),
    mail VARCHAR(" . TAM_MAIL . ")
)";
?>

<?php
//test de aceso a squile
try {
	
	//crear base de datos
	$conn = new PDO('sqlite:agenda.db');
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   // para mostrar los errores
	//$conn = new PDO('sqlite::memory:'); //base ed datos en memoria
	//creacion de la tabla
	$conn ->exec($crear_Tabla);
	

		
}catch(PDOException $e){
	echo $e->getMessage();
}

//cierra conexion
$conn = null;

?>