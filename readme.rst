Aplicación de Agenda PHP:
=========================

Esta aplicación gestiona los contactos de una agenda.

1-Para poder crear la base de datos, tenemos que ejecutar el fichero crear_bd.php desde el navegador

::

	http://localhost/~usuario/tareaagendaphp/crear_bd.php


2-Una vez realizado este paso ya podremos ver como funciona la agenda.

::

	http://localhost/~usuario/tareaagendaphp
	
3- Los usuarios para entrar desde el login de la aplicacion son:

	Usuario:root Contraseña:root
	
	Usuario:adrian Contraseña:adrian

Funcionalidad:
==============

Hay un menu de opciones donde podras acceder.

El usuario que este en la aplicacion podra:

	Podra añadir un contacto mediante un formulario
	
	Podra listar todos los contactos que tiene en la agenda

	Podra modificar los contactos
	
	Hay una opcion de buscar por nombre o apellido
	
	Podra borrar le contacto que desee
	
	Podra borrar todo el contenido de la agenda
